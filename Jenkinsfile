pipeline {
    agent any

    parameters {
        booleanParam(name: 'SHOULD_RELEASE', defaultValue: false, description: 'Indicates if this build should release (bumpversion, push tags, merge through to master, and push master)')
        string(name: 'BUMPVERSION', defaultValue: 'patch', description: 'The bumpversion section to bump (major, minor, or patch)')
    }

    stages {
        stage ('Build') {
            steps {
                sh 'docker build --tag genericstudent/base:latest .'
                dockerFingerprintFrom dockerfile: 'Dockerfile', image: 'genericstudent/base:latest'
            }
        }

        stage ('Release') {
            when {
                allOf {
                    branch 'development'
                    equals expected: true, actual: params.SHOULD_RELEASE
                }
            }
    
            steps {
                sh "echo \"bumpversion --verbose ${params.BUMPVERSION}\""
                sh 'echo "WOULD HAVE MERGED TO MASTER HERE"'
            }
        }

        stage ('Deploy') {
            when {
                branch 'master'
            }

            environment {
                DOCKER_LOGIN = credentials('docker-login')
            }

            steps {
                sh 'docker login --username $DOCKER_LOGIN_USR --password $DOCKER_LOGIN_PSW'
                sh 'docker push genericstudent/base:latest'
            }
        }
    }
}
